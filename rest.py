#REST stuff, because *nobody likes a library*.
#Actually, this isn't really rest stuff at all but useful for it I suppose..
from urllib import urlencode
import urllib2

#data and headers should be dicts ({"these" : "things"}) of param key/values
#returns a file-like response object
def post(url, data, headers = {}):
  opener = urllib2.build_opener()
  opener.addheaders = headers.items() #items() turns {1:2} into [(1, 2)]
  return opener.open(url, urlencode(data))
