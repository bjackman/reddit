#!/bin/python
import imgur
from datetime import datetime
import calendar
from urllib import urlopen
from urlparse import urlparse
from json import load as read_json
from time import sleep
from sys import argv

DELAY_SECONDS = 3

#ugh...
def get_utc_timestamp():
  d = datetime.utcnow()
  return calendar.timegm(d.utctimetuple())

def main():
  last_time = 1368731643
  while True:
    url = "http://www.reddit.com/domain/images.4chan.org/.json"
    connection = urlopen(url)
    json = read_json(connection)

    if not json["kind"] == "Listing":
      raise Exception("Unexpected 'kind': " + json["kind"])

    children = json["data"]["children"]

    #?sort=new returns them in the wrong order
    children.sort(key = lambda i: i["data"]["created_utc"], reverse=True)
    
    for post in children:
      data = post["data"]
      if data["created_utc"] > last_time:
        print("got a bite")
        print(data["url"])
        img = urlopen(data["url"]).read()
        imgur_link = imgur.upload_image(img)
        print(imgur_link)
        last_time = data["created_utc"]
      else:
        print(str(last_time) + " " + str(data["created_utc"]))

    sleep(DELAY_SECONDS)

if __name__ == "__main__":
  main()

