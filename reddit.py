import sys
import praw
from praw.errors import RateLimitExceeded, APIException
import imgur
from time import sleep
from urllib2 import urlopen, HTTPError

#format string to take two %strings: a url and a tag (i.e (NSFW))
COMMENT_FORMAT="""When you link to 4Chan, the link 404s after a short while.

[Here is an Imgur link to the image](%s). %s.

(I am a bot)"""

last_checked = 0

#ugh...
def get_utc_timestamp():
  d = datetime.utcnow()
  return calendar.timegm(d.utctimetuple())

def submission_contains_comment_by(submission, username):
  all_comments = praw.helpers.flatten_tree(submission.comments)
  #there must be some shorter way to do this.. I can't Python.
  for comment in all_comments:
    if comment.author.name == username:
      return True
  return False

def log(text):
  print(text)

def handle_ratelimit(function, *args, **kwargs):
  while True:
    try:
      return function(*args, **kwargs)
    except RateLimitExceeded as e:
      log("(rate limit exceeded in %s. sleeping %s seconds)" 
          % (function.__name__, e.sleep_time))
      sleep(e.sleep_time)
    
def handle_submission(submission):
  try:
    response = urlopen(submission.url)
    log("uploading %s from %s" % (submission.url, submission.permalink))
    imgur_url = imgur.upload_image(response.read())
    log("..successfully uploaded to " + imgur_url)
    tag = "(NSFW)" if submission.over_18 else ""
    comment_string = COMMENT_FORMAT % (imgur_url, tag)
    try:
      comment = handle_ratelimit(submission.add_comment, comment_string)
      log("..and commented to " + comment.permalink)
    except APIException as e:
      log("..couldn't post comment: " + str(e))
  except HTTPError:
    log("404: " + submission.url)

def main(username, ignore_nsfw=False):
  global last_checked
  while True:
    post_gen = reddit.get_domain_listing("images.4chan.org", sort="new")
    submission = post_gen.next()
    #(the posts will come in reverse time order.)
    new_last_checked = submission.created_utc
    while submission.created_utc > last_checked:
      #try to avoid duplicates.
      if submission_contains_comment_by(submission, username):
        log("skipping %s as it conains a comment by me." 
            % (submission.permalink))
      elif not ignore_nsfw or not submission.over_18:
        handle_submission(submission)
      else:
        log("ignoring NSFW post: " + submission.permalink)
      submission = post_gen.next()
    last_checked = new_last_checked
    log("Done, sleeping.")
    sleep(30)


if __name__ == "__main__":
  #global last_checked - nope, last_checked is already in scope here!
  #the utc timestamp of the last checked comment is stored in a file
  try:
    f = open(".last_checked", "r")
    last_checked = float(f.read())
    f.close()
  except IOError:
    last_checked = get_utc_timestamp()
  except ValueError:
    print("Warning, couldn't parse a float from .last_checked")
    last_checked = get_utc_timestamp()
    f.close()

  reddit = praw.Reddit(user_agent="4chan_relink_bot-/u/bjackman")

  try:
    uname, pword = open("reddit_secrets", "r").read().splitlines()
  except IOError:
    print("Couldn't open ./reddit_secrets!")

  reddit.login(uname, pword)

  #don't download NSFW links at school
  if len(sys.argv) > 1:
    if sys.argv[1] == "--no-nsfw":
      use_no_nsfw = True;
    else:
      print("Ignoring unrecognised argument: " + sys.argv[1])
  else:
    use_no_nsfw = False

  try:
    main(uname, use_no_nsfw)
  except Exception as e: #this will include KeyboardInterrupt
    log("Got an exception: " + str(e))
    f = open(".last_checked", "w")
    f.write(str(last_checked))
    f.close()
