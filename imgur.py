#Imgur stuff
from rest import post
from json import load as read_json #returns json as a dict ({these : things})

SECRETS_FILE = "./secrets"
ACCESS_TOKEN_REFRESH_URL = "https://api.imgur.com/oauth2/token"
IMAGE_UPLOAD_URL = "https://api.imgur.com/3/image"

#this will be done when this file gets imported - get keys and stuff
f = open(SECRETS_FILE)
[client_id, client_secret, refresh_token] = f.read().splitlines()
access_token = ""

#refresh token lasts forever, access token only 6 minutes
def get_access_token():
  data = {"refresh_token" : refresh_token, "client_id" : client_id, 
          "client_secret" : client_secret, "grant_type" : "refresh_token" }
  response = post(ACCESS_TOKEN_REFRESH_URL, data)
  return read_json(response)["access_token"]

#returns the "link" element in the response
def upload_image(image):
  access_token = get_access_token()
  params = {"image" : image}
  headers = {"Authorization" : "Bearer " + access_token}
  http_response = post(IMAGE_UPLOAD_URL, params, headers)
  response = read_json(http_response)
  if not "success" in response:
    print("Unexpected structure to upload response. Dumping.")
    print(response)
  elif not response["success"]:
    print("Failed to upload. Dumping response.")
    print(response)
  else:
    return response["data"]["link"]
